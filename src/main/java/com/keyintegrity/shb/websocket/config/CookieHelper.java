package com.keyintegrity.shb.websocket.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class CookieHelper {
    public static final String JWT_COOKIE_MODE = "COOKIES";
    public static final String JWT_TRANSFER_MODE_HEADER_NAME = "Jwt-Transfer-Mode";

    @Value("${cookies-domain:shb.local}")
    private String cookieDomain;


    public void addCookie(String cookieName, String cookieValue, int age,
                          boolean cookieHttpOnly, HttpServletResponse response) {
        Cookie cookie = new Cookie(cookieName, cookieValue);
        cookie.setMaxAge(age);
        cookie.setDomain(cookieDomain);
        cookie.setPath("/");
        if (cookieHttpOnly) cookie.setHttpOnly(true);
        response.addCookie(cookie);
    }

    /**
     * Определение режима передачи jwt токен через cookie.
     *
     * @return возвращает truе если в запросе установлен хедер Jwt-Transfer-Mode с значением COOKIES
     */

    public static boolean isCookieJwtTransferMode(HttpServletRequest request) {
        String headerValue = request.getHeader(JWT_TRANSFER_MODE_HEADER_NAME);
        return headerValue != null && JWT_COOKIE_MODE.equalsIgnoreCase(headerValue);
    }

}
