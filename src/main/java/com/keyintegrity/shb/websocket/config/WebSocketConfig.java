package com.keyintegrity.shb.websocket.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {
	@Value("${spring.rabbitmq.host:localhost}")
	private String rabbitHost;
	@Value("${spring.rabbitmq.stomp.port:61613}")
	private int rabbitStompPort;
	@Value("${spring.rabbitmq.client:guest}")
	private String client;
	@Value("${spring.rabbitmq.password:guest}")
	private String password;

	@Override
	public void configureMessageBroker(MessageBrokerRegistry config) {
		config
            .setApplicationDestinationPrefixes("/app")
			.enableStompBrokerRelay( "/topic")
			.setRelayHost(rabbitHost)
			.setRelayPort(rabbitStompPort)
			.setClientLogin(client)
			.setClientPasscode(password);
	}

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/websocket-app").withSockJS();
	}

}
