package com.keyintegrity.shb.websocket.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class JwtTokenFilter extends OncePerRequestFilter {
    @Value("${jwt.header-payload-cookie-name}")
    private String jwtHeaderPayloadCookieName;

    @Value("${jwt.header-payload-cookie-maxage}")
    private int jwtHeaderPayloadCookieMaxAge;

    @Value("${jwt.signature.cookie-name}")
    private String jwtSignatureCookieName;

    @Value("${jwt.authorization-header-name}")
    private String authHeaderName;

    @Autowired
    private CookieHelper cookieHelper;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        if(request.getHeader(authHeaderName) != null || request.getServletPath().contains("/oauth/token")) {
            filterChain.doFilter(request, response);
            return;
        }

        Cookie jwtDataPart = WebUtils.getCookie(request, jwtHeaderPayloadCookieName);
        Cookie jwtSignPart = WebUtils.getCookie(request, jwtSignatureCookieName);

        if (jwtDataPart != null && jwtSignPart != null) {
            if("GET".equals(request.getMethod()) || "POST".equals(request.getMethod())){
                // reconstruct JWT from cookies
                String authHeaderValue = String.format("Bearer %s.%s", jwtDataPart.getValue(), jwtSignPart.getValue());
                MutableHttpServletRequest mutableRequest = new MutableHttpServletRequest(request);
                mutableRequest.putHeader(authHeaderName, authHeaderValue);
                // update header.payload cookie max age
                cookieHelper.addCookie(jwtDataPart.getName(), jwtDataPart.getValue(), jwtHeaderPayloadCookieMaxAge, false, response);
                filterChain.doFilter(mutableRequest, response);
                return;
            }
        }

        filterChain.doFilter(request, response);
    }
}
