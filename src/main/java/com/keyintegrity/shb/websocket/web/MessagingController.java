package com.keyintegrity.shb.websocket.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessagingController {

	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	@MessageMapping("/message")
	@SendTo("/topic/CLIENT")
	public String send(String message) throws Exception {
 		return message;
	}


	@PostMapping(path = "/sendMessage")
	public void message(@RequestParam("type") String type, @RequestBody String payload) throws Exception {
		messagingTemplate.convertAndSend("/topic/"+type, payload);
	}

}
