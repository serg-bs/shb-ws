package com.keyintegrity.shb.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebsocketsRabbitmqApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebsocketsRabbitmqApplication.class, args);
	}
}
