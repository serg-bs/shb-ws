#! /bin/bash

ARTIFACT_NAME=`find . -name "shb-websocket-*"`

if [ "$1" = 'shb-websocket' ]; then
    java -Djava.security.egd=file:/dev/./urandom $JAVA_OPTS -jar $APP_HOME/$ARTIFACT_NAME
else
    exec "$@"
fi


