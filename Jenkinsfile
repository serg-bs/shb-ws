node {
    def app, appVersion, imgTag

    stage('Clone repository') {
        checkout scm
    }

    stage('Pull images') {
        docker.withRegistry('https://dc2.srvhub.tools', 'docker-registry-shb-deploy-credentials-id') {
            oracleImg = docker.image('dc2.srvhub.tools/shb/oracle_serverjre:8')
            oracleImg.pull()
        }
    }

    stage('Build image') {
        // list all env properties
        echo sh(script: 'env|sort', returnStdout: true)

        // get short git commit id
        shortCommitId = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
        echo "shortCommitId=${shortCommitId}"
        env.COMMIT_ID = shortCommitId

        sh "./gradlew clean buildDockerImage --stacktrace \
        -DMAJOR=${params.MAJOR} \
        -DMINOR=${params.MINOR} \
        -DPATCH=${params.PATCH} \
        -DSUFFIX=${params.SUFFIX} \
        -DBUILD_NUMBER=${env.BUILD_NUMBER} \
        -DCOMMIT_ID=${shortCommitId}"

        // read build-info.properties
        def buildProps = readProperties  file:'build/docker/build-info.properties'
        appVersion = buildProps["build.version"]
        echo "appVersion=${appVersion}"
        imgTag = buildProps["build.imgTag"]
        echo "imgTag=${imgTag}"

        app = docker.image("dc2.srvhub.tools/shb/shb-websocket:${imgTag}")

        sh "docker tag dc2.srvhub.tools/shb/shb-websocket:${imgTag} dc2.srvhub.tools/shb/${env.JOB_NAME}:latest"
    }

    stage('Test image') {
        // Todo
        app.inside {
            sh 'echo "Tests passed"'
        }
    }

    stage('Push image') {
        docker.withRegistry('https://dc2.srvhub.tools', 'docker-registry-shb-deploy-credentials-id') {
            app.push()
            docker.image("dc2.srvhub.tools/shb/${env.JOB_NAME}:latest").push()
        }
    }

}
